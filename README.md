# Cosmos

## General
Cosmos is flink + NGIS-v2 connector/agent. After deploy, you need to upload a JAR file to cosmos for every data stream that you want to do analytics on.

The Cosmos Generic Enabler simplifies Big Data analysis of context data and integrates with some of the many popular Big Data platforms.

Cosmos is a FIWARE Generic Enabler. Therefore, it can be integrated as part of any platform “Powered by FIWARE”. FIWARE is a curated framework of open source platform components which can be assembled together with other third-party platform components to accelerate the development of Smart Solutions.

This project is part of [FIWARE](https://www.fiware.org/). For more information check the FIWARE Catalogue entry for [Context Processing, Analysis and Visualization](https://github.com/Fiware/catalogue/tree/master/processing).

Original documentation can be found here: https://fiware-cosmos.readthedocs.io/en/latest/

## Usage of Cosmos
We don't use Cosmos, but provide a partial example of how to use within the [Installation How-To's](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/blob/master/howtos/CosmosConfig.md).

To learn more about using Flink, you can refer to official 'Getting Started' instructions: https://ci.apache.org/projects/flink/flink-docs-release-1.10/getting-started/
## Versioning  
Tested on:  
Version flink:1.9.0-scala_2.11 (DockerHub digest: sha256:7d33cf488b2f60375266359e2690f66e2a5a5bf9b71ccd7e7e56ed00f3c694dd)

## Volume Usage
flink-jobmanager uses a persistent directory for saving uploaded data (/opt/flink/tmpdir)  
PVC: NAMESPACE-flinkdata-STATEFULSET-NAME

## Load-Balancing
A component of this project, called flink-taskmanager, is replicated with 2 nodes.

## Route (DNS)
Cosmos / Flink UI can be accessed via: https://flink.fiware.opendata-CITY.de/

## Further Testing
The image is taken as is. No further component testing is done.

## Deployment
In order to deploy, following Gitlab-Variables and K8s-secrets are needed:
### Gitlab-Variables
KUBECONFIG - This variable contains the content of the Kube.cfg used to access the cluster for dev- and staging-stage.  
KUBECONFIG_PROD - This variable contains the content of the Kube.cfg used to access the cluster for prod-stage.  
NAMESPACE_DEV - This variable contains the namespace for dev-stage used by k8s  
NAMESPACE_STAGING - This variable contains the namespace for staging-stage used by k8s  
NAMESPACE_PROD - This variable contains the namespace for production-stage used by k8s  
### Kubernetes Secrets
This project does not use Kubernetes secrets.

## Funding Information
The results of this project were developed on behalf of the city of Paderborn within the funding of the digital model region Ostwestfalen-Lippe of the state of North Rhine-Westphalia.

![img](img/logoleiste.JPG)

## License
Copyright © 2020 Profirator Oy, HYPERTEGRITY AG, omp computer gmbh

This work is licensed under the EUPL 1.2. See [LICENSE](LICENSE) for additional information.
